package com.bangrawon.simple_fcm2.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.graphics.Color
import android.media.AudioAttributes
import android.net.Uri
import android.os.Build
import androidx.core.app.NotificationCompat
import com.bangrawon.simple_fcm2.R
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage


class MyFirebaseMessagingService :  FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {

        val channelId = "foreground_channel_id"
        val soundUri = Uri.parse("android.resource://" + applicationContext.packageName + "/" + R.raw.ringtone)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager


        val notification = NotificationCompat.Builder(this, channelId)
            .setContentTitle(remoteMessage?.notification!!.title)
            .setContentText(remoteMessage.notification!!.body)
            .setSmallIcon(R.drawable.logo)


        notification.setPriority(NotificationCompat.PRIORITY_HIGH)
            .setVibrate(longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400))
            .setChannelId(channelId)
            .setSound(soundUri)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            val mChannel = NotificationChannel(channelId, applicationContext.getString(R.string.app_name), NotificationManager.IMPORTANCE_HIGH)
            val attributes = AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .build()

            mChannel.description = "NJAJAL"
            mChannel.enableLights(true)
            mChannel.enableVibration(true)
            mChannel.lightColor = Color.RED
            mChannel.setSound(soundUri, attributes)
            mChannel.vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)
            notificationManager.createNotificationChannel(mChannel)
        }
        notificationManager.notify(Constant.NOTIFICATION_ID_FOREGROUND_SERVICE /* ID of notification */, notification.build())
    }

    override fun onNewToken(token: String) {
        //handle token
    }
}